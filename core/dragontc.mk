# Copyright (C) 2015-2016 DragonTC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Set Bluetooth Modules
BLUETOOTH := libbluetooth_jni bluetooth.mapsapi bluetooth.default bluetooth.mapsapi libbt-brcm_stack audio.a2dp.default libbt-brcm_gki libbt-utils libbt-qcom_sbc_decoder libbt-brcm_bta libbt-brcm_stack libbt-vendor libbtprofile libbtdevice libbtcore bdt bdtest libbt-hci libosi ositests libbluetooth_jni net_test_osi net_test_device net_test_btcore net_bdtool net_hci bdAddrLoader

#######################
##  D R A G O N T C  ##
#######################

# Disable modules that don't work with DragonTC. Split up by arch.
DISABLE_DTC_arm := libc_tzcode
DISABLE_DTC_arm64 := libstagefright_amrwbenc

# Set DISABLE_DTC based on arch
DISABLE_DTC := \
  $(DISABLE_DTC_$(TARGET_ARCH)) \
  $(LOCAL_DISABLE_DTC)

# Enable DragonTC on GCC modules. Split up by arch.
ENABLE_DTC_arm :=
ENABLE_DTC_arm64 :=

# Set ENABLE_DTC based on arch
ENABLE_DTC := \
  $(ENABLE_DTC_$(TARGET_ARCH)) \
  $(LOCAL_ENABLE_DTC)

# Enable DragonTC on current module if requested.
ifeq (1,$(words $(filter $(ENABLE_DTC),$(LOCAL_MODULE))))
  my_cc := $(CLANG)
  my_cxx := $(CLANG_CXX)
  my_clang := true
endif

# Disable DragonTC on current module if requested.
ifeq ($(my_clang),true)
  ifeq (1,$(words $(filter $(DISABLE_DTC),$(LOCAL_MODULE))))
    my_cc := $(AOSP_CLANG)
    my_cxx := $(AOSP_CLANG_CXX)
    ifeq ($(HOST_OS),darwin)
      # Darwin is really bad at dealing with idiv/sdiv. Don't use krait on Darwin.
      CLANG_CONFIG_arm_EXTRA_CFLAGS += -mcpu=cortex-a9
    else
      CLANG_CONFIG_arm_EXTRA_CFLAGS += -mcpu=krait
    endif
  else
    CLANG_CONFIG_arm_EXTRA_CFLAGS += -mcpu=krait2
  endif
endif


#################
##  P O L L Y  ##
#################

# Polly flags for use with Clang
POLLY := -O3 -mllvm -polly \
  -mllvm -polly-parallel -lgomp \
  -mllvm -polly-ast-use-context \
  -mllvm -polly-vectorizer=polly \
  -mllvm -polly-opt-fusion=max \
  -mllvm -polly-opt-maximize-bands=yes \
  -mllvm -polly-run-dce

# Enable version specific Polly flags.
ifeq (1,$(words $(filter 3.7 3.8 3.9,$(LLVM_PREBUILTS_VERSION))))
  POLLY += -mllvm -polly-dependences-computeout=0 \
    -mllvm -polly-dependences-analysis-type=value-based
endif
ifeq (1,$(words $(filter 3.8 3.9,$(LLVM_PREBUILTS_VERSION))))
  POLLY += -mllvm -polly-position=after-loopopt \
    -mllvm -polly-run-inliner \
    -mllvm -polly-detect-keep-going \
    -mllvm -polly-rtc-max-arrays-per-group=40
endif

# Disable modules that dont work with Polly. Split up by arch.
DISABLE_POLLY_arm := \
  libpng \
  libLLVMCodeGen \
  libLLVMARMCodeGen\
  libLLVMScalarOpts \
  libLLVMSupport \
  libLLVMMC \
  libLLVMMCParser \
  libminui \
  libgui \
  libF77blas \
  libF77blasAOSP \
  libRSCpuRef \
  libRS \
  libjni_latinime_common_static \
  libmedia \
  libRSDriver \
  libxml2 \
  libc_freebsd \
  libc_tzcode \
  libv8
DISABLE_POLLY_arm64 := \
  libpng \
  libfuse \
  libLLVMAsmParser \
  libLLVMBitReader \
  libLLVMCodeGen \
  libLLVMInstCombine \
  libLLVMMCParser \
  libLLVMSupport \
  libLLVMSelectionDAG \
  libLLVMTransformUtils \
  libF77blas \
  libbccSupport \
  libblas \
  libRS \
  libstagefright_mpeg2ts \
  bcc_strip_attr \
  libcrypto_static \
  libjpeg_static \
  libicuuc \
  libwebp-decode \
  libwebp-encode \
  libskia_static \
  libaudioutils \
  libpdfiumfpdfapi\
  libpdfiumfxge \
  libpdfiumfxcodec \
  libpdfiumfpdfdoc \
  libpdfiumjpeg \
  libpdfiumopenjpeg \
  libRS_internal \
  libcrypto \
  libopus \
  libyuv \
  libyuv_static \
  libjni_snapimageutil \
  libvpx \
  libaudioflinger \
  libsonic \
  libart \
  libscrypt_static \
  libstagefright_amrnbdec \
  libFFTEm \
  libbnnmlowp \
  libmedia_jni \
  libpixelflinger \
  libFraunhoferAAC \
  libavcenc \
  libmpeg2dec \
  libwebrtc_apm \
  libwebrtc_isac \
  libmusicbundle \
  libreverb

# Add version specific disables.
ifeq (1,$(words $(filter 3.8 3.9,$(LLVM_PREBUILTS_VERSION))))
  DISABLE_POLLY_arm64 += \
	healthd \
	libandroid_runtime \
	libblas \
	libF77blas \
	libF77blasV8 \
	libgui \
	libjni_latinime_common_static \
	libLLVMAArch64CodeGen \
	libLLVMARMCodeGen \
	libLLVMAnalysis \
	libLLVMScalarOpts \
	libLLVMCore \
	libLLVMInstrumentation \
	libLLVMipo \
	libLLVMMC \
	libLLVMSupport \
	libLLVMTransformObjCARC \
	libLLVMVectorize \
	libminui \
	libprotobuf-cpp-lite \
	libRS \
	libRSCpuRef \
	libunwind_llvm \
	libv8 \
	libvixl \
	libvterm \
	libxml2 \
	libavcdec
endif

# Set DISABLE_POLLY based on arch
DISABLE_POLLY := \
  $(DISABLE_POLLY_$(TARGET_ARCH)) \
  $(DISABLE_DTC) \
  $(BLUETOOTH) \
  $(LOCAL_DISABLE_POLLY)

# Set POLLY based on DISABLE_POLLy
ifeq (1,$(words $(filter $(DISABLE_POLLY),$(LOCAL_MODULE))))
  POLLY := -Os
endif

ifeq ($(my_clang),true)
  ifndef LOCAL_IS_HOST_MODULE
    # Possible conflicting flags will be filtered out to reduce argument
    # size and to prevent issues with locally set optimizations.
    my_cflags := $(filter-out -Wall -Werror -g -O3 -O2 -Os -O1 -O0 -Og -Oz -Wextra -Weverything,$(my_cflags))
    # Enable -O3 and Polly if not blacklisted, otherwise use -Os.
    my_cflags += $(POLLY) -Qunused-arguments -Wno-unknown-warning-option -w
  endif
endif


#############
##  L T O  ##
#############

# Disable modules that don't work with Link Time Optimizations. Split up by arch.
DISABLE_LTO_arm := libLLVMScalarOpts libjni_latinime_common_static libjni_latinime adbd nit libnetd_client libblas
DISABLE_THINLTO_arm := libart libart-compiler libsigchain
DISABLE_LTO_arm64 := gatekeeperd libart libFFTEm libpixelflinger libaudioflinger libc libEGL libaudioutils libandroid_runtime
DISABLE_THINLTO_arm64 := libkeymaster_messages gatekeeperd hostapd_cli hostapd idmap installd iotop libiprouteutil libnetlink ip ip6tables libipanat libnfnetlink libxml2 libnetfilter_conntrack ipacm iptables iw keystore_cli keystore_cli_v2 libsoftkeymaster keystore ld.mc linker libdefcontainer_jni lnkd lmkd libpcrecpp logcat logwrapper libsysutils logd libext4_utils make_ext4fs libmediacodecservice libcap libminijail mediacodec libmediadrm libstagefright_httplive libstagefright_wfd libmediaextractorservice libresourcemanagerservice libmediaplayerservice memory_replay mediaextractor mediadrmserver mediaserver memtest mke2fs libfuse mtpd mkfs.exfat mount.exfat ndc libqsap_sdk libmdnssd libnetdaidl netd libsigchain libvixl libart libart-compiler ping pngtest ping6 patchoat pppd libart-disassembler profman oatdump r radiooptions libkeystore-engine readmac readfem racoon reboot resize2fs librilutils schedtest libril run-as screencap rild screenrecord sdcard secdiscard service servicemanager libsensorservice sensorservice sgdisk sh ss libqdMetaData libqdutils libsurfaceflinger tinymix tc surfaceflinger toolbox tune2fs tracepath tracepath6 traceroute traceroute6 toybox tzdatacheck uncrypt vdc libf2fs_sparseblock libdiskconfig libcryptfs_hw libcryptfs wpa_cli vold wpa_supplicant ip-up-vpn libart_fake libjavacore libjavacrypto libkeymaster1 libsoftkeymasterdevice libkeystore_binder libstagefright_amrnb_common librtp_jni audio.primary.default libfwdlockengine audio.a2dp.default libtinycompress libtinyalsa audio.r_submix.default libaudioroute audio.primary.msm8996 libalsautils audio_policy.default audio.usb.default libqservice libmemalloc copybit.msm8996 libloc_stub gralloc.default libloc_pla libgps.utils libloc_core libloc_eng gps.msm8996 gralloc.msm8996 libsdmutils keystore.default local_time.default lights.msm8996 memtrack.msm8996 libsdmcore power.default nfc_nci.pn54x.default sensors.msm8996 vibrator.default hwcomposer.msm8996 libjnigraphics libOmxAacEnc libOmxAmrEnc libFFTEm libOmxCore libOmxEvrcEnc libOmxQcelp13Enc libOpenMAXAL libOmxVenc libOmxVdec libOpenSLES libQWiFiSoftApCfg libblas libandroid libRSCpuRef libRSDriver libinputservice libaudio-resampler libsuspend libandroid_servers libaudioeffect_jni libc_malloc_debug libcamera2ndk libcurl libdrmframework libdrmframework_jni libexif libfilterpack_imageproc libstdc++ libgatekeeper libgabi++ libgnsspps libmtp libnfc_ndef libmediandk libmedia_jni libopenjdkvm libopenjdkjvm libopenjdk libpagemap libpixelflinger libpac libreference-ril libshims_ims librs_jni librmnetctl libsoundpool libstagefright_soft_aacdec libstagefright_soft_aacenc libstagefright_soft_amrdec libstagefright_soft_amrnbenc libavcdec libstagefright_soft_avcdec libstagefright_soft_avcenc libstagefright_soft_flacenc libstagefright_soft_g711dec libstagefright_soft_gsmdec libstagefright_soft_mp3dec libstagefright_soft_hevcdec libstagefright_soft_mpeg2dec libstagefright_soft_mpeg4dec libstagefright_soft_rawdec libstagefright_soft_mpeg4enc libstagefright_soft_opusdec libstagefright_soft_vorbisdec libstagefright_soft_vpxdec libstagefrighthw libstagefright_soft_vpxenc libsurfaceflinger_ddmconnection libtinyxml libwebrtc_isac libaudiopreprocessing libmusicbundle libbundlewrapper libcyanogen-dsp libdownmix libeffectproxy libldnhncr libqcompostprocbundle libqcomvisualizer libqcomvoiceprocessor libqcomvoiceprocessing libreverb libvisualizer libreverbwrapper libvolumelistener libaudiospdif libaudioresampler libeffects libserviceutility libsonic libaudioflinger libaudiopolicyenginedefault libaudiopolicymanagerdefault libaudiopolicymanager libaudiopolicyservice libcameraservice libpower libradioservice libprotobuf-cpp-full libsoundtriggerservice libappfuse_jni power.msm8996 libdrmclearkeyplugin add-property-tag anrd antradio_app check-lost+found cpustats crypto dexlist dumpcache dnschk ksminfo fio httpurl latencytop librank micro_bench micro_bench_static mmc_utils procrank procmem perfprofd puncture_fs rawbu sane_schedstat showmap showslab sqlite3 simpleperf strace su taskstats tcpdump updater rmnetcli libstagefright_soft_amrwbenc libdl libc libnativehelper libjavacore-benchmarks libbacktrace libbacktrace_test libbinary_parse libimage_type_recognition liblzma libunwind libbinder backtrace_test libevent libjavacore-unit-tests memory_replay_tests libchrome bluetoothtbd_test libext2_uuid recovery libz libsparse libf2fs init adbd healthd libantradio libhardware libmemtrack libnativeloader libsync libui libexpat libinput libnetutils libEGL libGLESv2 libgui libandroidfw libwpa_client libcrypto libhardware_legacy libcamera_metadata libqinputflinger libinputflinger libcamera_client libpng libicuuc libicui18n libft2 libdng_sdk libtiff_directory libpiex libvulkan libGLESv1 libETC1 libsqlite libGLESv1_CM libpackagelistparser libpcre libselinux libsonivox libssl libpowermanager libspeexresampler libstagefright_foundation libaudioutils libusbhost libnbaio libharfbuzz_ng libmedia libnetd_client libradio_metadata libimg_utils libpdfium libradio libsoundtrigger libnativebridge libminikin  libprocessgroup libmemunreachable libprotobuf-cpp-lite libRScpp libbcinfo libRS_internal libRS libandroid_runtime bluetooth.default libbluetooth_jni libbt-vendor libjni_eglfence libjni_filtershow_filters libjni_jpegstream libnfc-nci libnfc_nci_jni libprintspooler_jni libjni_pacprocessor libframesequence libwebviewchromium_loader libgiftranscode libwebviewchromium_plat_support libmediautils libopus libstagefright_yuv libstagefright_omx libstagefright_enc_common libvorbisidec libstagefright_avc_common libstagefright libstagefright_http_support libwilhelm app_process arping atrace audiod libmedialogservice audioserver bcc libext2_com_err libext2_blkid libext2_e2p libext2fs bootanimation blkid bootstat bugreport bugreportz clatd cameraserver cmd dalvikvm debuggerd dexdump dhcptool dnsmasq drmserver dex2oat dumpstate dumpsys libext2_profile libext2_quota e2fsck libexfat fsck.f2fs fingerprintd fsck.exfat fsck_msdos gzip grep libhidcommand_jni libjni_snapimageutil libjni_snaptinyplanet libjni_snapmosaic

ENABLE_DTC_LTO := true

# Set DISABLE_LTO and DISABLE_THINLTO based on arch
DISABLE_LTO := \
  $(DISABLE_LTO_$(TARGET_ARCH)) \
  $(DISABLE_DTC) \
  $(LOCAL_DISABLE_LTO)
DISABLE_THINLTO := \
  $(DISABLE_THINLTO_$(TARGET_ARCH)) \
  $(LOCAL_DISABLE_THINLTO)

# Enable LTO (currently disabled due to issues in linking, enable at your own risk)
ifeq ($(ENABLE_DTC_LTO),true)
  ifeq ($(my_clang),true)
    ifndef LOCAL_IS_HOST_MODULE
      ifneq ($(LOCAL_MODULE_CLASS),STATIC_LIBRARIES)
        ifneq (1,$(words $(filter $(DISABLE_LTO),$(LOCAL_MODULE))))
          ifneq (1,$(words $(filter $(DISABLE_THINLTO),$(LOCAL_MODULE))))
            my_cflags += -flto=thin -fuse-ld=gold
            my_ldflags += -flto=thin -fuse-ld=gold
          else
            my_cflags += -flto -fuse-ld=gold
            my_ldflags += -flto -fuse-ld=gold
          endif
        else
          my_cflags += -fno-lto -fuse-ld=gold
          my_ldflags += -fno-lto -fuse-ld=gold
        endif
      endif
    endif
  endif
endif
